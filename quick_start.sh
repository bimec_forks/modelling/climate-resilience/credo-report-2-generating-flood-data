#!/bin/bash

echo "Podman or Docker?"
echo "1 - Podman"
echo "2 - Docker"
read -r ans

if [[ $ans == "1" ]]; then
    container="podman"
elif [[ $ans == "2" ]]; then
    container="docker"
else
    echo "Answer 1, or 2."
    exit 1
fi

echo "What do you want to run?"
echo "1 - build hipims container"
echo "2 - launch hipims container interactively"
echo "3 - run hipims on src/generic_hipims.py"
echo "4 - build and then run hipims on src/generic_hipims.py"
echo "5 - run jupyter lab in the hipims container to interrogate data folders"
read -r ans

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [[ $ans == "1" ]]; then
    $container build -t hipims-model .
elif [[ $ans == "2" ]]; then
    $container run --rm --env-file .env -v $SCRIPT_DIR/data:/data -it hipims-model bash
elif [[ $ans == "3" ]]; then
    $container run --rm --env-file .env -v $SCRIPT_DIR/data:/data hipims-model
elif [[ $ans == "4" ]]; then
    $container build -t hipims-model . \
    && $container run --rm --env-file .env -v $SCRIPT_DIR/data:/data hipims-model
elif [[ $ans == "5" ]]; then
    $container run --rm -p "127.0.0.1:8888:8888" --env-file .env -v $SCRIPT_DIR/data:/data -it hipims-model bash -c "cd /data && pip install jupyterlab && jupyter lab --allow-root --ip=0.0.0.0"
else
    echo "Answer 1 - 5."
    exit 1
fi
