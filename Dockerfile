# Base building image
FROM nvidia/cuda:11.4.1-devel-ubuntu20.04 AS builder

ENV CUDAToolkit_ROOT /usr/local/cuda-11.4
ENV PYTHONDONTWRITEBYTECODE=1

RUN apt-get -y update \
    && apt-get install --no-install-recommends -y python3-dev python3-pip build-essential git \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && pip3 install --no-compile --upgrade --no-cache-dir pip \
    && pip3 install --no-compile --no-cache-dir cmake

WORKDIR /builds

# Compile HiPIMS model
FROM builder AS build_hipims

RUN git clone https://github.com/HEMLab/hipims.git \
    && cd hipims \
    && git checkout c6231a1

WORKDIR /builds/hipims
RUN cmake . -DCMAKE_BUILD_TYPE=Release && make -j"$(nproc)"


# Build pypims wheel
FROM builder AS build_pypims

RUN git clone https://github.com/pypims/pypims.git \
    && cd pypims \
    && git checkout 236c089
WORKDIR /builds/pypims
RUN python3 setup.py bdist_dumb


# Build final container:
FROM nvidia/cuda:11.4.1-runtime-ubuntu20.04 as final

ENV CUDAToolkit_ROOT /usr/local/cuda-11.4
ENV PYTHONDONTWRITEBYTECODE=1

WORKDIR /hipims
RUN mkdir -p release/bin && mkdir -p /data/outputs

COPY requirements.txt .

RUN apt-get -y update \
    && apt-get install --no-install-recommends -y python3 python3-pip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && pip3 install --no-compile --no-cache-dir --upgrade pip \
    && pip3 install --no-compile --no-cache-dir rasterio \
    && pip3 install --no-compile --no-cache-dir -r requirements.txt \
    && rm requirements.txt \
    && find / -depth \
        \( \
            \( -type d -name __pycache__ \) \
            -o \( -type f -a \( -name '*.pyc' -o -name '*.pyo' -o -name '*.whl' \) \) \
        \) -exec rm -rfv '{}' +

COPY --from=build_pypims /builds/pypims/dist/pypims-0.0.3.0.linux-x86_64.tar.gz .
RUN tar -xvf pypims-0.0.3.0.linux-x86_64.tar.gz -C / && rm pypims-0.0.3.0.linux-x86_64.tar.gz

# copy built HiPIMS model over
COPY --from=build_hipims /builds/hipims/release release

# Copy model script over
COPY src /hipims/src

CMD ["python3", "src/generic_hipims.py"]
