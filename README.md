# HiPIMS flood modelling on DAFNI

DAFNI model to run [HiPIMS](https://github.com/HEMLab/hipims) for a given set
of inputs, using [pypims](https://pypi.org/project/pypims/) for simplicity.

Developed under the National Digital Twin Programme's Climate Resilience
Demonstrator ([CReDo](https://digitaltwinhub.co.uk/projects/credo/what-is-credo/)) project and released for public use in support of future
climate resilience data generation.  
  
For more details on this work, see the report either on the Digital Twin hub ([CReDo Technical Report 2: Generating flood data](https://digitaltwinhub.co.uk/files/file/122-credo-technical-report-2-generating-flood-data/)) or the [DOI](https://doi.org/10.17863/CAM.81781) for that document.  

- [HiPIMS flood modelling on DAFNI](#hipims-flood-modelling-on-dafni)
- [Running on DAFNI](#running-on-dafni)
  - [Input dataslots](#input-dataslots)
    - [Digital elevation model (mandatory)](#digital-elevation-model-mandatory)
    - [Rain source (mandatory)](#rain-source-mandatory)
    - [Rain mask (optional)](#rain-mask-optional)
    - [Land cover (optional)](#land-cover-optional)
    - [Manning Coefficients (optional)](#manning-coefficients-optional)
    - [Gauge positions (optional)](#gauge-positions-optional)
    - [Waterbody boundaries (optional)](#waterbody-boundaries-optional)
- [Running and/or developing elsewhere](#running-andor-developing-elsewhere)
  - [Local deployment](#local-deployment)
  - [Uploading new model versions to DAFNI](#uploading-new-model-versions-to-dafni)

# Running on DAFNI

For detailed information about the input/output data required/produced for a
single model run, please see the
[HiPIMS Manual](https://github.com/HEMLab/hipims/blob/master/Userguide.md).

Our DAFNI model wraps the [HiPIMS](https://github.com/HEMLab/hipims)
models in a docker image, and uses the [pypims](https://github.com/pypims/pypims)
python package to run the model on provided input data.

This docker image is built and exported to DAFNI for public use
[here](https://facility.secure.dafni.rl.ac.uk/models/details?version_id=7d0df466-4e08-483e-8473-cb9964e5a479).
To request a DAFNI account, please use the [sign-up form](https://dafni.ac.uk/accessing-dafni/).

## Input dataslots

There are a number of input dataslots available, two of which are mandatory.

Details are provided below, along with example file contents to give an
illustration of the expected format of their contents.

Each of these files must be uploaded as a separate dataset on DAFNI; the model
wrapper code will then look for a single file from each dataslot and use it if
available (for optional inputs), or fail if not (for mandatory inputs).

### Digital elevation model (mandatory)

Digital Elevation Model in a single asc/gz/tif file.

Projected CRS, units: m

Example:

```bash
ncols    3
nrows    3
xllcorner    0
yllcorner    0
cellsize    10
NODATA_value    -9999
-9999 -9999 10.0386
-9999 -3.1825 2.2100
-10.6575 -11.0725 -10.835
```

### Rain source (mandatory)

Rain source csv file.

First column represents time step in seconds.
Subsequent columns represent rainfall in m/s for each catchment
(i.e. timestep, catchment_0, catchment_1, ..., catchment_N).

Note the inclusion of a 0 rainfall column here (the second column in our
example) to cover areas without rainfall, rather than using the NODATA value in
the [rain mask](#rain-mask-optional).

Example:

```bash
0,0,1.38889e-05,0,1.38889e-06
360,0,1.38889e-05,0,1.38889e-06
720,0,1.38889e-05,0,1.38889e-06
1080,0,1.38889e-05,0,1.38889e-06
1440,0,1.38889e-05,0,1.38889e-06
1800,0,1.38889e-05,0,1.38889e-06
2160,0,1.38889e-05,0,1.38889e-06
2520,0,1.38889e-05,0,1.38889e-06
2880,0,1.38889e-05,0,1.38889e-06
3240,0,1.38889e-05,0,1.38889e-06
```

### Rain mask (optional)

Rain mask in a single asc/gz/tif file, with the same CRS as DEM file.

Rather than using the NODATA value, assign to group 0 and then
provide 0 rainfall in the corresponding rain_source column.

Example:

```bash
ncols    3
nrows    3
xllcorner    0
yllcorner    0
cellsize    10
NODATA_value    -9999
0 0 1
2 0 1
2 3 1
```

### Land cover (optional)

Land cover data in a single asc/gz/tif file, with the same CRS as DEM file.

Use integers to denote land cover types - these can also then be assigned
manning coefficients via the [Manning Coefficients](#manning-coefficients-optional)
input file.

Example:

```bash
ncols    3
nrows    3
xllcorner    0
yllcorner    0
cellsize    10
NODATA_value    -9999
1 0 -9999
0 1 0
0 0 1
```

### Manning Coefficients (optional)

JSON file containing manning coefficients for each class of land in the
provided [Land Cover](#land-cover-optional) data.

Manning lookup dictionary should have keys matching land use classes (as
strings, not integers).

Example:

```json
{
    "manning_lookup": {
    "0": 0.035,
    "1": 0.0055
    },
    "default": 0.035
}
```

### Gauge positions (optional)

CSV file containing gauge/monitor positions.

This file should have two columns:

- x position
- y position

Any headers in the first row of the file will be ignored, and can be renamed
for more suitable alternatives as needed.

Example:

```csv
X,Y
0,0
1,1
2,2
3,3
```

### Waterbody boundaries (optional)

JSON file containing boundary locations/conditions, defined by coordinate pairs
of the diametrically opposing corners of their bounding boxes

Example:

```json
{
  "boundaries": [
    {
      "name": "box_upstream",
      "polyPoints": [
        [1427, 195],
        [1446, 243]
      ],
      "type": "open",
      "hU": [
        [0, 100],
        [3600, 100]
      ]
    },
    {
      "name": "box_downstream",
      "polyPoints": [
        [58, 1645],
        [72, 1170]
      ],
      "type": "open",
      "h": [
        [0, 5],
        [3600, 5]
      ]
    }
  ]
}
```

# Running and/or developing elsewhere

As this model is containerised, it is easy enough to run on other platforms outside
DAFNI (though you may suffer from much longer run-times if your local hardware
is not similarly capable).

## Local deployment

Requires a podman or docker installation.

We've included a [quick start](quick_start.sh) script to get you up and
running easily enough - this mounts our data input and output folders in the
same locations as used on DAFNI model runs, so you can complete local model
runs before pushing the next model version up to DAFNI.

To get started, in a terminal window type:

```bash
bash ./quick_start.sh
```

and follow the prompts from there.

## Uploading new model versions to DAFNI

To create a new model release, run:

```bash
make clean # to remove old builds
make build # note: this may take a few minutes as the image is large!
```

A new `hipims-model.tar.gz` file will be created in the `dafni_models`
directory; upload this along with the [model definition file](hipims.yaml) to
DAFNI and you're good to go.
